let server  = require('http').Server()

let io = require('socket.io')(server)

let Redis = require('ioredis')

let redis = new Redis();

redis.subscribe('test-channel')

redis.on('message', function (channel, message) {
    message = JSON.parse(message)

    io.emit(`${channel}:${message.event}`, message.data )
})

io.on('connection', (socket) => {
    socket.on('test', (data) => {
        console.log(data)
    })

    socket.on('disconnect', (data) => {
        console.log(data)
    })
})

server.listen(3000);
